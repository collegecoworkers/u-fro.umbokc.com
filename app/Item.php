<?php
namespace App;

class Item extends MyModel{
	public function getGender() {
		return self::getGenderOf($this->gender);
	}

	public static function getGenders(){
		return [
			'm' => 'Мужчина',
			'f' => 'Женщина',
		];
	}

	public static function getGenderOf($r){
		$statuses = self::getGenders();
		if(array_key_exists($r, $statuses)) 
			return $statuses[$r];
		return $statuses['in'];
	}

	public function parentsCount() {
		$count = 0;
		$items = explode(',', $this->parents);
		F::filterEmpty($items);
		foreach ($items as $item) {
			$count++;
		}
		return $count;
	}

	public function setParents($id) {
		$items = explode(',', $this->parents);
		F::filterEmpty($items);
		$items[] = $id;
		$this->parents = implode(',', $items);
	}
}
