<?php
namespace App;

class Tree extends MyModel{
	private static $getItems_data = [];

	public static function getItemsDataOf($key_id) {
		$data = [];
		$data_ = self::getItemsOf($key_id);
		foreach ($data_ as $item) {
			$data[$item->id] = [
				'item' => $item->name,
				'type' => $item->type,
				'gender' => $item->gender,
				'age' => $item->age,
			];
			if($item->parents != ''){
				$parents = explode(',', $item->parents);
				F::filterEmpty($parents);
				$data[$item->id]['parents'] = $parents;
			}
		}
		return $data;
	}

	public static function getItemsOf($key_id) {
		self::$getItems_data[] = self::getItem(Item::getBy('id', $key_id));
		F::filterEmpty(self::$getItems_data);
		$data = self::$getItems_data;
		self::$getItems_data = [];
		return $data;
	}

	private static function getItem($item) {
		if ($item) {
			$parents = explode(',', $item->parents);
			F::filterEmpty($parents);
			foreach ($parents as $id) {
				self::$getItems_data[] = self::getItem(Item::getBy('id', $id));
			}
			return $item;
		}
		return null;
	}

	public function countItems() {
		return count(self::getItemsOf($this->key_id));
	}
}
