<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Tree,
	Item
};

class SiteController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Index() {
		return view('index')->with([
			'user' => User::curr(),
			'trees' => (User::isAdmin() ? Tree::all() : Tree::getsBy('user_id', User::curr()->id)),
		]);
	}

	public function Tree($id) {
		$tree = Tree::getBy('id', $id);
		$items = Tree::getItemsOf($tree->key_id);
		return view('tree')->with([
			'tree' => $tree,
			'items' => $items,
		]);
	}
}
