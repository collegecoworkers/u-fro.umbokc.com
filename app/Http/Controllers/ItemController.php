<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	User,
	Tree,
	Item
};

class ItemController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function Add($id) {
		return view('item.add')->with([
			'id' => $id,
			'genders' => Item::getGenders(),
			'user' => User::curr(),
		]);
	}
	public function AddParent($id) {
		return view('item.add-parent')->with([
			'item_id' => $id,
			'genders' => Item::getGenders(),
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$model = Item::getBy('id', $id);
		return view('item.edit')->with([
			'genders' => Item::getGenders(),
			'model' => $model,
		]);
	}
	public function Delete($id) {
		Item::where('id', $id)->delete();
		return redirect()->to('/');
	}
	public function Create($id, Request $request) {

		$tree = Tree::getById($id);
		$model = new Item();

		$model->name = request()->name;
		$model->type = request()->type;
		$model->gender = request()->gender;
		$model->age = request()->age;
		$model->parents = '';
		$model->save();

		$tree->key_id = $model->id;
		$tree->save();

		return redirect()->to('tree/list/'.$id);
	}
	public function CreateParent($id, Request $request) {

		$item = Item::getById($id);
		$model = new Item();

		$model->name = request()->name;
		$model->type = request()->type;
		$model->gender = request()->gender;
		$model->age = request()->age;
		$model->parents = '';
		$model->save();

		$item->setParents($model->id);
		$item->save();

		return redirect()->to('/');
	}
	public function Update($id, Request $request) {
		$model = Item::getBy('id', $id);

		$model->name = request()->name;
		$model->type = request()->type;
		$model->gender = request()->gender;
		$model->age = request()->age;

		$model->save();
		return redirect()->to('/');
	}
}
