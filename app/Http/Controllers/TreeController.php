<?php

namespace App\Http\Controllers;

use PDF;
use Illuminate\Http\Request;
use App\{
	User,
	Tree,
	Item
};

class TreeController extends Controller
{

	public function __construct(){
		$this->middleware('auth');
	}

	public function View($id) {
		$tree = Tree::getById($id);
		$name = $tree->title;

		// $name = $tree->title.'.pdf';
		// $data = view('tree.view')->with(['items' => Tree::getItemsOf($tree->key_id)]);
		// $html = '<title>' . $name . '</title>' . $data;

		// return PDF::loadHTML($html)
		// 			->setPaper('a4', 'landscape')
		// 			->setWarnings(false)
		// 			->stream($name);

		return view('tree.view')->with([
			'key' => $tree->key_id,
			'title' => $name,
			'data' => Tree::getItemsDataOf($tree->key_id),
		]);
	}

	public function Add() {
		return view('tree.add')->with([
			'user' => User::curr(),
		]);
	}
	public function Edit($id) {
		$model = Tree::getBy('id', $id);
		return view('tree.edit')->with([
			'model' => $model,
		]);
	}
	public function Delete($id) {
		Tree::where('id', $id)->delete();
		return redirect()->to('/');
	}
	public function Create(Request $request) {
		$model = new Tree();

		$model->title = request()->title;
		$model->key_id = 0;
		$model->user_id = User::curr()->id;

		$model->save();
		return redirect()->to('/');
	}
	public function Update($id, Request $request) {
		$model = Tree::getBy('id', $id);

		$model->title = request()->title;

		$model->save();
		return redirect()->to('/');
	}
}
