<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'SiteController@Index')->name('/');
Route::get('/tree/list/{id}', 'SiteController@Tree')->name('/tree/list/{id}');

Route::get('/tree/view/{id}', 'TreeController@View')->name('/tree/view/{id}');
Route::get('/tree/add', 'TreeController@Add')->name('/tree/add');
Route::get('/tree/edit/{id}', 'TreeController@Edit')->name('/tree/edit/{id}');
Route::get('/tree/delete/{id}', 'TreeController@Delete')->name('/tree/delete/{id}');
Route::post('/tree/create', 'TreeController@Create')->name('/tree/create');
Route::post('/tree/update/{id}', 'TreeController@Update')->name('/tree/update/{id}');

Route::get('/item/add/{id}', 'ItemController@Add')->name('/item/add/{id}');
Route::get('/item/edit/{id}', 'ItemController@Edit')->name('/item/edit/{id}');
Route::get('/item/delete/{id}', 'ItemController@Delete')->name('/item/delete/{id}');
Route::post('/item/create/{id}', 'ItemController@Create')->name('/item/create/{id}');
Route::post('/item/update/{id}', 'ItemController@Update')->name('/item/update/{id}');

Route::get('/item/add-parent/{id}', 'ItemController@AddParent')->name('/item/add-parent/{id}');
Route::post('/item/create-parent/{id}', 'ItemController@CreateParent')->name('/item/create-parent/{id}');

Route::get('/users', 'UserController@Users')->name('/users');
Route::get('/user/add', 'UserController@Add')->name('/user/add');
Route::get('/user/edit/{id}', 'UserController@Edit')->name('/user/edit/{id}');
Route::get('/user/delete/{id}', 'UserController@Delete')->name('/user/delete/{id}');
Route::post('/user/create', 'UserController@Create')->name('/user/create');
Route::post('/user/update/{id}', 'UserController@Update')->name('/user/update/{id}');
