{!! Form::open(['url' => isset($model) ? '/user/update/'.$model->id : '/user/create', 'class' => '']) !!}
<div class="account-top">
	<div><span>Имя</span>{!! Form::text('full_name', isset($model) ? $model->full_name : '', ['required' => '', 'class' => 'form-control']) !!}</div>
	<div><span>Логин</span>{!! Form::text('name', isset($model) ? $model->name : '', ['required' => '', 'class' => 'form-control']) !!}</div>
	<div><span>Email</span>{!! Form::text('email', isset($model) ? $model->email : '', ['required' => '', 'class' => 'form-control']) !!}</div>
	<div><span>Роль</span>{!! Form::select('role', $roles, isset($model) ? $model->role : '', ['required' => '', 'class' => 'form-control']) !!}</div>
	<div><span>Пароль</span>{!! Form::password('password', ['class' => 'form-control']) !!}</div>
</div>
<br>
<div class="account-top">
	<input type="submit" class="" value="Сохранить" cur:p>
</div>
{!! Form::close() !!}
