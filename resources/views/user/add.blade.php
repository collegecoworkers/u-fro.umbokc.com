@extends('../layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2>Новый пользователь</h1>
		@include('user._form')
	</div>
</div>
@endsection
