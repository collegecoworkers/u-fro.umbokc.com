{!! Form::open([
  'url' => isset($model) ? '/item/update/'.$model->id : 
  (!isset($item_id) ? '/item/create/' . $id : '/item/create-parent/' . $item_id),
  'class' => ''
  ]) !!}
	<div class="form-group">
    <label>Имя</label>{!! Form::text('name', isset($model) ? $model->name : '', ['class' => 'form-control']) !!}
    <label>Тип</label>{!! Form::text('type', isset($model) ? $model->type : '', ['placeholder' => 'Пример: Мама, Папа', 'class' => 'form-control']) !!}
    <label>Возраст</label>{!! Form::text('age', isset($model) ? $model->age : '', ['class' => 'form-control']) !!}
		<label>Пол</label>{!! Form::select('gender', $genders, isset($model) ? $model->gender : '', ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary" value="Сохранить">
	</div>
{!! Form::close() !!}
