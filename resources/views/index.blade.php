@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-head-line">
				Ваши деревья
			</h1>
			<a href="{{ route('/tree/add') }}" class="btn btn-primary">Добавить</a>
			<br>
			<br>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Деревья
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Название</th>
									<th>Элементов</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($trees as $item)
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->title }}</td>
										<td>{{ $item->countItems() }}</td>
										<td>
											<a href="{{ route('/tree/view/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-eye"></i>
											</a>
											<a href="{{ route('/tree/list/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-list"></i>
											</a>
											<a href="{{ route('/tree/edit/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ route('/tree/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
