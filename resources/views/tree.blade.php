@extends('layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-head-line">
				{{ $tree-> title}}
			</h1>
			@if ($tree->countItems() <= 0)
				<a href="{{ route('/item/add/{id}', ['id'=>$tree->id]) }}" class="btn btn-primary">Добавить</a>
				<br><br>
			@endif
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Элементы
				</div>
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-striped table-bordered table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>Имя</th>
									<th>Тип</th>
									<th>Родители</th>
									<th>Действия</th>
								</tr>
							</thead>
							<tbody>
								@php
								@endphp
								@foreach ($items as $item)
									@continue(!isset($item))
									<tr>
										<td>{{ $item->id }}</td>
										<td>{{ $item->name }}</td>
										<td>{{ $item->type }}</td>
										<td>
											@php
												$count = $item->parentsCount();
												echo $count;
												if($count < 2){
													echo "<a href=\"".route('/item/add-parent/{id}', ['id'=>$item->id]) . "\"><i class=\"fa fa-plus\"></i></a>";
												}
											@endphp
										</td>
										<td>
											<a href="{{ route('/item/edit/{id}', ['id'=>$item->id]) }}">
												<i class="fa fa-pencil"></i>
											</a>
											<a href="{{ route('/item/delete/{id}', ['id'=>$item->id]) }}" onclick="return confirm('Вы уверенны?')">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
