<!DOCTYPE HTML>
<html ea>
<head>
	<title>{{ config('app.name', 'Laravel') }}</title>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />

	<link href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3" rel="stylesheet">

	<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />

	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<div class="navbar navbar-inverse set-radius-zero">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand logo-a" href="/">
					{{ config('app.name', 'Laravel') }}
				</a>
			</div>
			<div class="left-div">
				<i class="fa fa-users login-icon" ></i>
			</div>
		</div>
	</div>
	<div class="content-wrapper">
		@yield('content')
	</div>
	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.
				</div>
			</div>
		</div>
	</footer>

	<script src="{{ asset('assets/js/jquery-1.11.1.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.js') }}"></script>
</body>
</html>
