@php
	use App\User;
@endphp
@extends('layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2 ta:l>Личный кабинет: заявки</h2>
		<div class="product-top">
			<a href="{{ route('/product/add') }}" class="btn btn-primary">Добавить</a>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">#</th>
						<th scope="col">Пользователь</th>
						<th scope="col">Цена</th>
						<th scope="col">Действия</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($bids as $item)
						<tr>
							<td>{{$item->id}}</td>
							<td>{{ User::getById($item->user_id)->full_name }}</td>
							<td>${{ $item->price}}</td>
							<td>
								<a href="{{ route('/product/check/{id}', ['id'=>$item->id]) }}">
									<i class="fa fa-check"></i>
								</a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			<div class="clearfix"> </div>
		</div>	
	</div>
</div>
@endsection
