@extends('../layouts.auth')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 class="page-head-line">Пожалуйста Войдите, Чтобы Войти</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<form method="post" action="{{ route('login') }}">
				{{ csrf_field() }}
				<label>Email: </label>
				<input name="email" placeholder="Email" type="text" class="form-control" />
				@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
				<label>Пароль :  </label>
				<input name="password" placeholder="Пароль" type="password" class="form-control" />
				@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				<hr />
				<button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Войти </button>&nbsp;
				<a href="{{ route('register') }}">Регистрация</a>
			</form>
		</div>
		<div class="col-md-6">
		</div>
	</div>
</div>
@endsection
