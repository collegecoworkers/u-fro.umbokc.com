@extends('../layouts.auth')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h4 class="page-head-line">Пожалуйста зарегистрируйтесь, чтобы войти</h4>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<form method="post" action="{{ route('register') }}">
				{{ csrf_field() }}
				<label>Полное имя</label>
				<input name="full_name" placeholder="Полное имя" type="text" class="form-control">
				@if ($errors->has('full_name'))<span class="help-block"><strong c#f>{{ $errors->first('full_name') }}</strong></span>@endif
				<label>Логин</label>
				<input name="name" placeholder="Логин" type="text" class="form-control">
				@if ($errors->has('name'))<span class="help-block"><strong c#f>{{ $errors->first('name') }}</strong></span>@endif
				<label>Email</label>
				<input name="email" placeholder="Email" type="text" class="form-control">
				@if ($errors->has('email'))<span class="help-block"><strong c#f>{{ $errors->first('email') }}</strong></span>@endif
				<label>Пароль</label>
				<input name="password" placeholder="Пароль" type="password" class="form-control">
				@if ($errors->has('password'))<span class="help-block"><strong c#f>{{ $errors->first('password') }}</strong></span>@endif
				<label>Пароль еще раз</label>
				<input name="password_confirmation" placeholder="Пароль еще раз" type="password" class="form-control">
				@if ($errors->has('password_confirmation'))<span class="help-block"><strong c#f>{{ $errors->first('password_confirmation') }}</strong></span>@endif
				<hr />
				<button type="submit" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> &nbsp;Зарегистрироваться </button>&nbsp;
				<a href="{{ route('login') }}">Войти</a>
			</form>
		</div>
		<div class="col-md-6">
		</div>
	</div>
</div>

@endsection
