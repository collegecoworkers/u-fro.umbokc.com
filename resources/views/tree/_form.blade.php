{!! Form::open(['url' => isset($model) ? '/tree/update/'.$model->id : '/tree/create', 'class' => '']) !!}
	<div class="form-group">
		<label>Название дерево</label>
		{!! Form::text('title', isset($model) ? $model->title : '', ['class' => 'form-control']) !!}
	</div>
	<div class="form-group">
		<input type="submit" class="btn btn-primary" value="Сохранить">
	</div>
{!! Form::close() !!}
