@php
	class TreeGen {
		private static $used = [];
		private static $data = [];
		private static $main = '';

		public static function set($main = '', $data = []){
			self::$main = $main;
			self::$data = $data;
			return self::getHtml();
		}

		private static function getItem($item){
			$gender = [ 'f' => 'Женщина', 'm' => 'Мужчина'];
			$res = '';
			$res .= "<li>";
			$gen = $item['gender'];

			$res .= '<a href="/" ta:l '. ($gen == 'm' ? 'gen-m' : 'gen-f') .">";
			$res .= $item['item'];
			if($item['type'] != '') $res .= "<span><b>Тип: </b>{$item['type']}</span>";
			if($item['age'] != '') $res .= "<span><b>Возраст: </b>{$item['age']}</span>";
			$res .= "<span><b>Пол: </b>{$gender[$gen]}</span>";
			$res .= "</a>";

			if (array_key_exists('parents', $item)){
				$parents = $item['parents'];
				$res .= '<ul>';
				foreach ($parents as $id) {
					$n_item = self::$data[$id];
					$res .= self::getItem($n_item);
					self::$used[] = $id;
				}
				$res .= '</ul>';
			}

			$res .= "</li>";
			return $res;  
		}

		private static function getHtml(){
			$res = '';
			if (!empty(self::$data)) {
				$key = self::$main;
				$res .= "<ul>";
				$res .= self::getItem(self::$data[$key]);
				$res .= "</ul>";
				self::$used[] = $key;
			}
			// print_r(self::$used);die;
			return $res;
		}
	}
@endphp
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>{{ $title }}</title>
	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=1.3">
	<link rel="stylesheet" href="{{ asset('assets/style.css') }}">
</head>
<body ea>
	<div class="tree" id="tree">
		{!! TreeGen::set($key, $data) !!}
	</div>
</body>
</html>
