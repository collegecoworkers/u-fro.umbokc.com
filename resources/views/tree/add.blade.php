@extends('../layouts.app')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1 class="page-head-line">
				Новое дерево
			</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Новое дерево
				</div>
				<div class="panel-body">
					@include('tree._form')
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
