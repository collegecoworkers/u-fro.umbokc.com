<script src="//cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
{!! Form::open(['url' => isset($model) ? '/product/update/'.$model->id : '/product/create', 'class' => '']) !!}
<br>
<div class="account-top">
	<span>Название</span>
	{!! Form::text('title', isset($model) ? $model->title : '') !!}
</div>
<br>
<div class="account-top">
	<span>Начальная цена</span>
	{!! Form::text('curr_price', isset($model) ? $model->curr_price : '') !!}
</div>
<br>
<div class="account-top">
	<span>Категория</span>
	{!! Form::select('cat_id', $cats, isset($model) ? $model->cat_id : '', ['w:100p']) !!}
</div>
<br>
<div class="account-top">
	<span>Описание</span>
</div>
<div>
	<textarea id="editor1" name="desc" rows="40">{{ isset($model) ? $model->desc : ''}}</textarea>
</div>
<div class="account-top">
	<input type="submit" class="" value="Сохранить" cur:p>
</div>
{!! Form::close() !!}
<script>
	CKEDITOR.replace( 'editor1' );
</script>
