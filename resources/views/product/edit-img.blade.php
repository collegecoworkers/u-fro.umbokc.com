@extends('../layouts.app')
@section('content')
<div class="product-grids">
	<div class="container">
		<h2>Изменить товар</h2>
    {!! Form::open(['url' => '/product/update-img/'.$model->id , 'class' => '', 'enctype'=>'multipart/form-data']) !!}
    <div class="account-top">
      <span>Изображение</span>
      {!! Form::file('image', ['placeholder' => 'Изображение','class' => 'form-control']) !!}
    </div>
    <div class="account-top">
      <input type="submit" class="" value="Сохранить" cur:p>
    </div>
    {!! Form::close() !!}
	</div>
</div>
@endsection
