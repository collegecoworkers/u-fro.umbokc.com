@extends('../layouts.app')
@section('content')
<div class="row">
	<h2>Добавить новую статью</h2>
	@include('post._form')
</div>
@endsection
