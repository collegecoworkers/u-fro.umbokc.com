<script src="//cdn.ckeditor.com/4.9.2/full/ckeditor.js"></script>
{!! Form::open(['url' => isset($model) ? '/post/update/'.$model->id : '/post/create', 'class' => '']) !!}
<br>
Название:
<br>
<input w:100p style="" p:v:0 class="text" type="text" name="title" value="{{ isset($model) ? $model->title : ''}}" />
<br>
Хештеги:
<input w:100p style="" p:v:0 class="text" placeholder="Через пробел" type="text" name="hashtags" value="{{ isset($model) ? $model->title : ''}}" />
<br>
Краткое описание:
<textarea name="desc">{{ isset($model) ? $model->desc : ''}}</textarea>
<br>
Книга:
<textarea id="editor1" name="data" rows="40">{{ isset($model) ? $model->data : ''}}</textarea>

<br>
<button type="submit" class="button">Сохранить</button>
{!! Form::close() !!}
<script>
	CKEDITOR.replace( 'editor1' );
</script>
